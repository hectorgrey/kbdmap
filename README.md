# kbdmap

## Description
An implementation of keyboard layouts

## Usage
`kbdmap <layout>`

## License
MIT License

## Project status
Very much work in progress.  This might be slow as I'm a beginner at this kind of thing.  Code review and constructive critique welcome.
