use serde::{Deserialize, Serialize};
use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

#[derive(Debug, Serialize, Deserialize)]
struct KeyMap {
    pub map: Vec<Vec<char>>,
}

impl KeyMap {
    pub fn new_from_file(filename: &str) -> Self {
        let file = File::open(filename).unwrap();
        let mut buf_read = BufReader::new(file);
        let mut contents = String::new();
        buf_read.read_to_string(&mut contents).unwrap();
        ron::from_str(&contents).unwrap()
    }
}

fn main() {
    let uk = KeyMap::new_from_file("uk.ron");
    let mut keymap_array = [['\0'; 4]; 87];
    let mut key_index = 0;
    for key in uk.map {
        let mut symbol_index = 0;
        for symbol in key {
            keymap_array[key_index][symbol_index] = symbol;
            symbol_index += 1;
        }
        key_index += 1;
    }
    println!("{:?}", keymap_array);
}
